<div align="center"><img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/SoftwareHelperLogo.png"/></div>   

##  Welcome to SoftwareHelper

### [Github](https://github.com/WPFDevelopersOrg) https://github.com/WPFDevelopersOrg  

### [码云](https://gitee.com/WPFDevelopersOrg) https://gitee.com/WPFDevelopersOrg  

![dotnet-version](https://img.shields.io/badge/.net%20framework-%E2%89%A54.0-blue)  ![Visual Studio 2019](https://img.shields.io/badge/Visual%20Studio%20-2019-blueviolet)  <a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=B61RFy2vvpaKLEDxaW6NsDpPZA-eSyFh&jump_from=webapi"><img border="0" src="https://pub.idqqimg.com/wpa/images/group.png" alt="WPF开发者" title="WPF开发者"></a> [![码云](https://img.shields.io/badge/Gitee-%E7%A0%81%E4%BA%91-orange)](https://gitee.com/yanjinhua/SoftwareHelper.git)   [![Github](https://img.shields.io/badge/%20-github-%2324292e)](https://github.com/yanjinhuagood/SoftwareHelper)   


#### 此项目大家互相学习，互相进步。方便的话给一个<kbd>Star</kbd>，谢谢！

### 欢迎关注微信公众号  
<img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/wxgzh.jpg"/>     

----------
### 捐助
如果您觉得我们的开源软件对你有所帮助，请扫下方二维码打赏我们一杯咖啡。
| 微信 | 支付宝 |
|----|-----|
|  <img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/Alipay.png"/>   |   <img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/WeChatPay.png"/>   |

### **下载安装后体验 [releases](https://github.com/yanjinhuagood/SoftwareHelper/releases/)**   


### __效果图预览如下↓↓↓↓↓↓__   


#### 自动更新  

<img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/SoftwareHelperResource/AutoUpdater.gif"/>  

----------

#### 启动页  

<img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/SoftwareHelperResource/GIFfree.gif"/>  
<img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/SoftwareHelperResource/free1.png"/>  

----------

#### 颜色拾取

<img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/SoftwareHelperResource/GIFColor.gif"/>  

----------

#### 可以自行定义 C:\Users\用户\AppData\Local\SoftwareHelper\application.json 路径修改

<img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/SoftwareHelperResource/jsonconfig.png"/>  

----------

#### 搜索定位功能 <kbd>LeftAlt</kbd>+<kbd>（应用首字的首字母）</kbd>（应用首字的首字母）

<img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/SoftwareHelperResource/KeyBoardEntry.png"/>  

----------

#### 启动选择 __嵌入|悬浮__ 

<img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/SoftwareHelperResource/desktop.gif"/>  
<img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/SoftwareHelperResource/select.png"/>  

----------

#### 是否边缘隐藏 

<img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/SoftwareHelperResource/IsEdgeHide.png"/>  

----------

#### 系统应用

<img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/SoftwareHelperResource/systemAppliction.png"/>  

----------

#### 移除应用

<img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/SoftwareHelperResource/Remove.png"/>  

----------

#### 移动应用顺序

<img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/SoftwareHelperResource/drag.png"/>  

----------

#### 托盘、换肤、透明度

<img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/SoftwareHelperResource/original.png"/>  
<img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/SoftwareHelperResource/MheelGif.gif"/>  
<img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/SoftwareHelperResource/GIFNew.gif"/>  
<img src="https://github.com/WPFDevelopersOrg/ResourcesCache/raw/main/resources/SoftwareHelperResource/GIFMini.gif"/>  

----------

